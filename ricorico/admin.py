# -*-coding: utf-8 -*-

from django.contrib import admin
from models import *

# Configurar lista de recetas en la administración
class RecetaAdmin(admin.ModelAdmin):
  # Los campos que queremos mostrar
  list_display = ('nombre', 'fecha_creacion', 'fecha_edicion')
  # Crear filtros de fecha
  list_filter = ['fecha_creacion', 'fecha_edicion']
  # Crear campo de busqueda por nombre
  search_fields = ['nombre']
  # Excluimos el campo de userid del form ya que se hará automaticamente
  exclude = ['userid']

  # Sobreescribimos el método para guardar la ID del usuario actual
  def save_model(self, request, obj, form, change):
    if not change:
      obj.userid = request.user
    obj.save()

  # Sobreescribimos el método de listar para que
  # solo muestre los que esa persona ha creado
  def queryset(self, request):
    if request.user.is_superuser:
      return Receta.objects.all()
    return Receta.objects.filter(userid=request.user)

  # Ya que el anterior método solo los pone en la lista,
  # con este nos aseguramos que tenga permisos para cambiarlo
  def has_change_permission(self, request, obj=None):
    has_class_permission = super(RecetaAdmin, self).has_change_permission(request, obj)
    if not has_class_permission:
      return False
    if obj is not None and not request.user.is_superuser and request.user.id != obj.userid.id:
      return False
    return True

# Añadimos los 2 modelos al menú de administración
admin.site.register(Ingrediente)
admin.site.register(Receta, RecetaAdmin)