from django.db import models
from django.contrib.auth.models import User, Group, Permission

# Creamos los grupos y permisos para los usuarios
"""normal_users = Group(name='Normal Users')
normal_users.save()"""

# Creamos los modelos para la bbdd
class Ingrediente(models.Model):
  nombre = models.CharField(max_length=256)

  def __unicode__(self):
    return self.nombre

class Receta(models.Model):
  tipos = (('Entrante', 'Entrante'),
          ('Primero', 'Primero'),
          ('Segundo', 'Segundo'),
          ('Postre','Postre'))

  userid = models.ForeignKey(User)
  nombre = models.CharField(max_length=256)
  tipo = models.CharField(max_length=256, choices=tipos)
  ingredientes = models.ManyToManyField(Ingrediente)
  elaboracion = models.TextField()
  tiempo = models.CharField(max_length=256)
  fecha_creacion = models.DateTimeField(auto_now_add=True)
  fecha_edicion = models.DateTimeField(auto_now=True)
  imagen = models.ImageField(upload_to='images/platos', blank=True)

  def __unicode__(self):
    return self.nombre