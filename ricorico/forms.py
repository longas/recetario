from django import forms
from models import *

class SearchForm(forms.Form):
  tipos = (('ninguno', '-----'),
          ('Entrante', 'Entrante'),
          ('Primero', 'Primero'),
          ('Segundo', 'Segundo'),
          ('Postre','Postre'))

  nombre = forms.CharField(required=False)
  tipo = forms.ChoiceField(required=False, choices=tipos)
  ingrediente = forms.ModelChoiceField(required=False, queryset=Ingrediente.objects.all())
  autor = forms.ModelChoiceField(required=False, queryset=User.objects.all())