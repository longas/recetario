# -*-coding: utf-8 -*-

from django import forms
from django.contrib import auth
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponseRedirect
from django.shortcuts import render, render_to_response
from django.views import generic
from django.template.context import RequestContext
from models import *
from forms import *

""" Uso generic views para mayor comodidad """
""" https://docs.djangoproject.com/en/1.6/topics/class-based-views/ """
class IndexView(generic.ListView):
  template_name = 'list.html'
  context_object_name = 'receta_list'

  def get_queryset(self):
    """ Devolvemos la lista de recetas ordenadas """
    return Receta.objects.order_by('-fecha_creacion')

class DetailView(generic.DetailView):
  model = Receta
  template_name = 'detail.html'

""" Volvemos al uso de vistas funcionales """
def register(request):
  if request.method == 'POST':
    form = UserCreationForm(request.POST)

    if form.is_valid():
      # Guardamos el nombre y password del form
      nombre = form.cleaned_data.get('username')
      password = form.cleaned_data.get('password1')

      # Creamos un nuevo usuario
      new_user = User.objects.create_user(username=nombre, password=password)
      # Le damos permisos para poder acceder al admin
      new_user.is_staff = True;
      # Le damos permisos para crear, editar y borrar
      add_ingrediente = Permission.objects.get(codename = "add_ingrediente")
      change_ingrediente = Permission.objects.get(codename = "change_ingrediente")
      delete_ingrediente = Permission.objects.get(codename = "delete_ingrediente")
      add_receta = Permission.objects.get(codename = "add_receta")
      change_receta = Permission.objects.get(codename = "change_receta")
      delete_receta = Permission.objects.get(codename = "delete_receta")
      new_user.user_permissions.add(add_ingrediente, change_ingrediente, delete_ingrediente, add_receta, change_receta, delete_receta)
      # Lo guardamos
      new_user.save()

      # Lo logeamos
      new_user = authenticate(username=nombre, password=password)
      login(request, new_user)

      return HttpResponseRedirect("/recetas")
  else:
    form = UserCreationForm()

  #return render(request, "registration.html", {'form': form})
  return render_to_response('registration.html', {'form': form}, context_instance=RequestContext(request))

def logout(request):
  auth.logout(request)
  return HttpResponseRedirect("/recetas")
  #return redirect('index')

def search(request):
  if request.method == 'POST':
    form = SearchForm(request.POST)

    if form.is_valid():
      nombre = form.cleaned_data.get('nombre')
      tipo = form.cleaned_data.get('tipo')
      ingrediente = form.cleaned_data.get('ingrediente')
      autor = form.cleaned_data.get('autor')

      # Si ningún campo ha sido rellenado no busca
      if not nombre and tipo == 'ninguno' and ingrediente is None and autor is None:
        return HttpResponseRedirect('/recetas')

      # Creamos la lista de todas las recetas para luego ir filtrando.
      # No supone perdida de rendimiento ya que hasta que no se haya terminado
      # la query no se ejecuta nada.
      receta_list = Receta.objects.all()

      # Comprobamos los campos y vamos stackeando la query
      if nombre:
        receta_list = receta_list.filter(nombre__icontains=nombre)

      if tipo != 'ninguno':
        receta_list = receta_list.filter(tipo=tipo)

      if ingrediente is not None:
        receta_list = receta_list.filter(ingredientes=ingrediente)

      if autor is not None:
        # Obtengo el usuario
        autorObject = User.objects.get(username=autor)
        # Lo filtro por id
        receta_list = receta_list.filter(userid=autorObject.id)

      return render(request, 'list.html', {'receta_list': receta_list})

  form = SearchForm()
  return render(request, 'buscar.html', {'form': form})