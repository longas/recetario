Recetario con Django
====================
Gabriel Longás
--------------

### Instalación y configuración
* Instalamos virtualenv para poder crear entornos de desarrollo de Python (**$ sudo pip install virtualenv**)
* Creamos nuestro entorno (**$ virtualenv recetario**) y lo inicializamos (**$ . recetario/bin/activate**)
* Creamos una carpeta llamada app dentro de tareas y metemos ahí los archivos
* Instalamos las depencias (**$ pip install -r requirements.txt**)
* Creamos la BBDD (**$ python manage.py syncdb**)
* Ejecutamos el servidor (**$ python manage.py runserver**)
* Entramos a nuestro navegador y nos metemos en **localhost:8000**