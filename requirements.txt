Django==1.6.1
Pillow==2.3.0
argparse==1.2.1
django-grappelli==2.5.1
wsgiref==0.1.2