from django.conf.urls import patterns, include, url
from ricorico import views

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
  url(r'^grappelli/', include('grappelli.urls')),
  url(r'^admin/', include(admin.site.urls)),
  url(r'^$', views.IndexView.as_view()),
  url(r'^recetas/', include('ricorico.urls'), name='recetas'),
  url(r'^registration/$', views.register, name='auth_register'),
  url(r'^logout/$', views.logout, name='auth_logout')
)
